﻿using System.Net;
using ShellProgressBar;
class Program
{
    static async Task Main(string[] args)
    {
        await DownloadAsync(args[0], args[1]);
        Console.WriteLine("\n");
        Console.WriteLine("Download complete");
    }

    static async Task DownloadAsync(string source, string dest)
    {
        var options = new ProgressBarOptions
        {
            ProgressCharacter = '█',
            ProgressBarOnBottom = true
        };
        const int totalTicks = 100;
        using (WebClient wc = new WebClient())
        {
            var pbar = new ProgressBar(totalTicks, "Initial message", options);
            wc.DownloadProgressChanged += (s, e) => { pbar.Tick(e.ProgressPercentage,$"Download file {e.BytesReceived}/{e.TotalBytesToReceive}"); };
            await wc.DownloadFileTaskAsync(new Uri(source),dest);
        };
    }
}
